+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "dark" # can influence the text color
particles = false
title_page = true 
minHeight = "500px"

title = "Offer Help In The Trails End Area"
#subtitle = ""

[header]
  image = "hands-together-banner.jpg"

#[asset]
  #image = "logo.svg"
  #width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Offer Help"
  url = "/offer/#contact"
  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary
+++
