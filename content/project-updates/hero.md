+++
fragment = "hero"
disabled = true
date = "2016-09-07"
weight = 50
background = "light" # can influence the text color
particles = false
title_page = false 
minHeight = "500px"
background_position = "center center"
background_size = "contain"

[header]
  image = "library-of-things-why-buy.png"

+++
