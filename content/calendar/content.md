---
fragment: "content"
# disabled: = false
date: "2021-06-16"
weight: 100
# background: "success"
# title: "About Syna"
# subtitle: ""
---

## Visit the calendar in your browser
If you want to visit the calendar in your browser you can bookmark this link. [https://cloud.trailsend.online/apps/calendar/p/fmN8eXLjD5dHgQBa](https://cloud.trailsend.online/apps/calendar/p/fmN8eXLjD5dHgQBa)

## Add the calendar to your phone 
### iPhone 
1. Go to Settings > Calendar > Accounts > Add Account > Other.  
1. Tap Add Subscribed Calendar.  
1. Enter the URL `https://cloud.trailsend.online/remote.php/dav/public-calendars/fmN8eXLjD5dHgQBa?export` to subscribe.  

### Android / Google Calendar
1. On your computer, open Google Calendar.  
1. On the left, next to "Other calendars," click Add Add other calendars and then From URL.  
1. Enter the following URL `https://cloud.trailsend.online/remote.php/dav/public-calendars/fmN8eXLjD5dHgQBa?export`.  
1. Click Add calendar. The calendar appears on the left, under "Other calendars."  

## Add the calendar to your computer
### Thunderbird
1. From the Home tab, choose Create a new calendar > On the network.  
1. Choose iCalendar (ICS).  
1. Paste the Calendar URL `https://cloud.trailsend.online/remote.php/dav/public-calendars/fmN8eXLjD5dHgQBa?export` in the Location field.  
1. Name the calendar.

### Outlook
1. Open Outlook and go to your calendar.  
1. In the sidebar, right-click "Other Calendars" and then select Add Calendar > From Internet.  
1. In the box that pops up, paste in the URL `https://cloud.trailsend.online/remote.php/dav/public-calendars/fmN8eXLjD5dHgQBa?export` and click "OK."  