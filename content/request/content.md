+++
fragment = "content"
#disabled = false
date = "2017-10-05"
weight = 100
#background = ""

#title = "About Syna"
#subtitle = ""
+++

## IF THIS IS AN EMERGENCY CALL 911
---

### What should you do if you have COVID-19 symptoms?

#### STAY HOME AND AVOID CONTACT WITH OTHERS

**AND** Call your primary care provider

---

You may also fill out the form below to get connected with other people in our local area.

The information that you enter is confidential, and will not be resold or redistributed for commercial purposes.