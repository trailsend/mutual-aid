+++
fragment = "nav"
#disabled = true
date = "2018-05-17"
weight = 0
background = "light"

[repo_button]
  url = "https://gitlab.com/trailsend/mutual-aid"
  text = "Fork" # default: "Star"
  icon = "fab fa-gitlab" # defaults: "fab fa-github"

# Branding options
[asset]
  image = "mutual-aid.jpg"
  text = "Community Mutual Aid Network"
+++
