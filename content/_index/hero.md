+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "dark" # can influence the text color
particles = false
title_page = true 
minHeight = "500px"

title = "Hi Neighbor"
subtitle = "This site was setup to track community projects."

[header]
  image = "header.jpg"

#[asset]
  #image = "logo.svg"
  #width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Project Updates"
  url = "/project-updates"
  color = "success"
+++
