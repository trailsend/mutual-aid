+++
title = "Gardening"
weight = 40

[asset]
  icon = "fas fa-seedling"
+++

Knowing where your food is coming from is rewarding and there is nothing that gets you more in touch with that as gardening. Are you interested in starting a personal or community garden?