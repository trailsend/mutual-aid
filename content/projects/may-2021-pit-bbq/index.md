+++
fragment = "content"
weight = 120

header = "Community Pit BBQ"
title = "You're invited to a community BBQ."
date = "2021-06-09"
categories = ["Event", "BBQ"]
+++

## Thanks for all the hard work organizing and preparing
There was a lot of hard work put into organizing the event and cleaning and preparing the area for the BBQ. You know who you are and we appreciate all the labor and love you put into making it happen.  

## Schedule of Events
### Friday 6/11
* 8am - Butchering of pig  
* 8pm - Movie night 

### Saturday 6/12
* 9am - Coffee
* 6pm - Bonfire starts  
* 8pm - Tents put up for those wanting to camp overnight  
* 8pm - Movie starts  
* 10pm - Placing food in the pit and burying  

### Sunday 6/13
* 10am - Setup tables  
* 11am - Dig up BBQ and pull out food  
* 12pm - Start eating  

## What to bring (Suggestions)
* Warm clothes & bug protection if you will be attending any of the night events.  
* Chairs for yourself and your guests  
* Side dish along with any utensils needed to server your side.  
* Drinks  
* $10 donation (If there is anything left over after reimbursing expenses it will be held for future events, we will report here all the expenses once they come in and total donations collected)  


## Expenses 
* $200 pig 
* $199 Porta potty

## Donations collected
$176
