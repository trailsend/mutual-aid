+++
fragment = "content"
weight = 120

header = "Community Cloud"
title = "Access to calendars, shared documents, communication."
date = "2021-06-19"
categories = ["Technology", "Communication", "Organizing"]
+++

## What is it?
We are using [NextCloud](https://nextcloud.com/) a self-hosted cloud platform like GSuite. Which gives users the ability to share documents, calendars, contacts, chat.

## Why use NextCloud?
* Self-hosted: means that the community controls the data and access.  
* Open source: meaning the source code is completely open for analysis of how the system functions, allows you to make updates and customizations, usage of the software is free.  
* Abundance of apps: allows to freely upgrade to have different abilities.  

## How to access?
* You can access content that is shared publicly by others.  
* You can sign up for access to store documents, update calendars, have access to contacts. [Trailsend Cloud](https://cloud.trailsend.online/apps/registration/)  

## Where is it hosted?
Currently the software is hosted on private server of a community member Marvin Roman. If at some point we have a technology fund we can migrate to a community server.  

## Access from your phone
If you want to have access to the community cloud via your phone there are 2 potential apps that you might want to download and sign into once you have created an account. 
1. Nextcloud Files [iPhone](https://itunes.apple.com/us/app/nextcloud/id1125420102?mt=8) [Google Play](https://play.google.com/store/apps/details?id=com.nextcloud.client) [F-Droid](https://f-droid.org/packages/com.nextcloud.client/)  
1. Nextcloud Talk [iPhone](https://play.google.com/store/apps/details?id=com.nextcloud.talk2) [Google Play](https://play.google.com/store/apps/details?id=com.nextcloud.talk2) [F-Droid](https://f-droid.org/en/packages/com.nextcloud.talk2)