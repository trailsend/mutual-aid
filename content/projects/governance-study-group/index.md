+++
fragment = "content"
weight = 120

header = "Governance Study Group"
title = "Studying governance models, meeting facilitation, group decision making."
date = "2020-07-22"
categories = ["Governance", "Organizing", "Conflict Mitigation"]
+++

There are lots of ideas that float around about community projects, but without proper governance they will likely not come to fruition. So the first step in moving forward with community projects is to study modern governance models and organizations/communitites that are agile, resilient and egalitarian. 

<!--more-->

## What is Governance?
Governance is often thought about as the legacy operating system that runs our government institutions, is cumbersome, and often devolves into a popularity or power contest. However, informal governance is used in our everyday lives, whenever families decide on rules, friends decide on group activities, companies decide on policy. So when we talk about governance we talk about how can we better make decisions together as groups and make sure that everybody has their voice heard and conflict is minimized because we become intentional in refocusing our listening for what works for the group instead of personal preference. 

## Why not Majority Rules/Representative Democracy?
In majority rules democracy the group that is first past the post, or the group with the most votes wins. This means that minority opinions does not have to be listened to or incorporated into the final decisions. This often leads to a fracturing of the populace as each group tries to become the majority and get their preferred policies enacted irregardless of minority objections.  
Major failures of majority rules democracy:  
1. In first past the post elections you don't even need to have a majority to win, you could have only 30% of the vote and still have the power to rule over the remainder of the populace that did not vote for you.  
1. Majority can overrule minority objections which can lead to oppressive decisions made by the majority.  
1. It tends to lead to fracturing of social cohesion and partisanship as each group fights to move from the minority to the majority, each side becoming more reactionary to the other side and less critical of their own.  

### Why not Representative Democracy?
1. In representative democracy, elected representatives are chosen to make rules that people are governed by. This often leads to dissatisfaction as people are left out of the decision making process in areas of life that in which they have material interests.  
1. It is a top down & centralized decision making process, which means that often decisions are made by a person that has material interests that are the opposite of their constituency, and very distant from the effects.

## Alternative Voting Methods
1. **Consent** - everybody must consent that the proposal or elected member is good enough to move forward with now. 
    * **Benefits**  
        * Focus on coming to decisions that are in everybodies range of tolerance rather than meeting everybodies preference (less egocentric).  
        * Decisions are meant to be term limited and revisited to be improved upon or scrapped.  
        * Nothing can move forward if a member objects, and objections are reframed as a gift to improve upon proposals.  
    * **Downfalls** 
        * Works only in small groups, this can be overcome through organizational structures like sociocracy or other methods of connecting federated groups.  
1. **Consensus** - everybody must agree to the decision before it moves forward.
    * **Benefits**  
        * No voice is left behind and the best decision is often arrived at.  
    * **Downfalls**  
        * Usually only works in groups with a lot of shared values.  
        * Usually only works in small groups.  
        * Can fail to come to a decision since it doesn't exit the egotistical realm of preference.  
1. **Ranked Choice Voting** - each choice is ranked in order of preference by each member and then there is an iterative method of finding the most preferred candidate or proposal.
    * **Benefits**  
        * Better represents populaces preferences.  
        * Encourages more solutions or electors to be considered.  
    * **Downfalls** 
        * Still majority rules and minority objections can be ignored.  


## What is Sociocracy or Dynamic Governance?
Sociocracy is a governance (Operating System) system for groups that is inclusive, bottom up, decentralized and scalable from families to national organizations. For a better idea of what Sociocracy is, you can watch this 19 minute introduction video included at the bottom of the page. [What is Sociocracy?](https://www.sociocracyforall.org/what-is-sociocracy-19min-animated-video/).

### Advantages of Sociocracy
* Decisions are based on consent. Meaning it may not be the preferred decision of all members, but it is within their range of tolerance and nobody objects to it.  
* Decisions are not considered permanent but are good enough to move forward with at the moment, to be revisited and improved upon after feedback and metrics come in.  
* Everybody's voice matters and every objection matters. This transforms the listening of the group from avoiding objections as hindering progress to valuing objections as chances to improve upon proposals/decisions.  
* Authority is moved to the edges instead of centralized. This means that if your group is responsible for results you are also responsible for the decisions that affect the outcome of those results.  
* Dominance is discouraged and those that attempt patterns of dominance that are a part of our regular lives are actively discouraged in the system and would likely never result in election to any leadership role.  
* Organizational structures are well thought out and can be duplicated and adopted.  
