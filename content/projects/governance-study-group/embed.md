+++
fragment = "embed"
#disabled = false
date = "2020-07-24"
weight = 150
background = "success"

#title = "What is Sociocracy"
#subtitle = "Easily embed media (videos, iframes etc.)"
#title_align = "left" # Default is center, can be left, right or center

media_source = "https://www.youtube.com/embed/b6r3-s2p7eI"
#media = '<iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/lXpf4kIxygU?rel=0&amp;showinfo=0" allowfullscreen></iframe>'
#ratio = "16by9" # 21by9, 16by9, 4by3, 1by1 - Default: 4by3
#size = "100" # 25, 50, 75, 100 (percentage) - default: 75
+++
