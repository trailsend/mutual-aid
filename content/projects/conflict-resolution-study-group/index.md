+++
fragment = "content"
weight = 110

header = "Conflict Resolution Study Group"
title = "Studying conflict resolution strategies and communication strategies to minimize conflict."
date = "2021-04-22"
categories = ["Organizing", "Conflict Mitigation"]
+++

We unfortunately aren't well trained or practiced in communicating well with those we disagree with. This prevents wider cooperation and sharing hard to accomplish. So how do we improve these skill sets and gain confidence in not shying away from conflict but moving towards it to resolve issues and find unity beneath the surface of the conflict? 

<!--more-->

## Currently Reading
[Nonviolent communication](https://www.nonviolentcommunication.com/) by Marshal Rosenberg 