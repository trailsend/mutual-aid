+++
fragment = "content"
weight = 100
#disabled = true
header = "Fire Suppression"
title = "Neighborhood Fire Suppression and Fire Break Maintenance"
date = "2021-04-26"
categories = ["Fire Suppression"]
+++

Maintain & extend the firebreak around the community and help homeowners with major fire suppression projects.  
<!--more-->

## Purpose 
Maintain & extend the firebreak around the community and help homeowners with major fire suppression projects.  


## Progress
### 8/22/2021
We were able to put in about 12 cummulative hours on the North end of Fir St. And made quite a bit of progress in much of the brush along the road.
Some before and after photos.
{{< beforeafter before="IMG_2724.JPG" after="IMG_2741.JPG" caption="" >}}
{{< beforeafter before="IMG_2731.JPG" after="IMG_2745.JPG" caption="" >}}


### 8/26/2021 
After a lot of work on the chipper by Steve Wellman, including rebuilding the carberator and replacing several parts it is now up and running smoothly and ready to do a lot of chipping.

### Financial costs/donations
Total costs for repairs so far if you wish to make a donation towards these costs let Patrick or Marvin know.
* $10.18 new ignition (paid by Dan Roman).  
* $28 new starter solenoid (paid by Patrick Roman). 
* $25.85 new starter solenoid (second replacement) (paid by Marvin Roman).  
* $46.69 carberatuer kit (paid by Marvin Roman).  
* $300 mechanic labor costs ($100 paid by Marvin Roman, $200 paid by Patrick Roman).  
* $30 new fuel pump (paid by Patrick Roman).  
* ($100) donation from Red Dewese (re-embursed Patrick Roman).  
* ($40) donation from Red Dewese (re-embursed Marvin Roman).  
* $253.40 New chipper blades (paid for by Marvin Roman).  
* ($100) donation from Red Dewese (re-embursed Marvin Roman).  
* $17.53 5 gallons fuel for chipper (paid for by Marvin Roman).  
* ($33) donation from David (re-embursed Marvin Roman).  
* ($100) donation from Ruth & Rick (re-embursed Marvin Roman).  


## Action Items
* Inventory community member tools and determine acquisitions to make.  
    * [Spreadsheet Tab 1](https://docs.google.com/spreadsheets/d/1EdGU-Xk-zjRK5EDaNC7IHi2U39LUvRuEx8mwta95gIc/edit?usp=sharing)  
* Inventory community members available time, then create & schedule work crews. 
    * [Spreadsheet Tab 2](https://docs.google.com/spreadsheets/d/1EdGU-Xk-zjRK5EDaNC7IHi2U39LUvRuEx8mwta95gIc/edit?usp=sharing)  
* Contact Mountain Communities Fire Safe Council to find what resources they have to help us organize and fund our project. 
    * Contacted on 4/27/2021 and discussed our goals. They are in the middle of grant writing, but said they would call back this week to discuss more in depth. If no call back by Monday 5/3 will reach out again.    
    * Brought up with meeting with Forest Service on 4/28/2021 regarding cattle and grazing. The Forest Service representative will be putting us in touch with somebody from San Diego who has organized something similar in her community.  
    * Received call back on 5/3 from Ruby.  
        * She said they are investigating whether we fall within their territory and if so they may have funds available for fire abatement on personal property. They don't currently handle anything on Forest Service property, but will reach out to their contacts in Forest Service to ask what avenues might be available for us.  
        * She will also be gathering some materials to send to us. She hopes to be getting in contact soon with more information. 
        * I let her know some of the avenues we are looking at and some of our goals more in depth.  
* Contact the contractor that is clearing the Girl Scout property and get a bid for the East hill of the community.  
    * Talked with Ed Arredondo. 
        * He gave some pointers on dealing with the Forest Service and attempting to get them to come do Fire Abatement around the community.  We should try and get a letter together signed with goals of what we would like to see and signatures from residents of the community.  We should try and cultivate a relationship with the local mountain ranger since they have the most sway in determining funding for projects like this.  
        * He gave tips on fire abatement and we should probably invite him up to walk the neighborhood and look at the personal property and find tips and strategies and also understand what he might charge for larger abatement projects.
        * We need to look up property lines and understand where property lines especially on the East side of the neighborhood are. Residents along the East end may be able to reclaim a lot of their property by down fire abatement along the backside of their property.  
* Create a work plan.  
* Coordinate tool maintenance prior to starting project.  
* Contact Station 53 fire captain about walking the neighborhood to make recommendations for personal property fire suppression.  

## Desired Roles
* Communications - communicate updates, determine communications channels, reach out to members and outside organizations.  
* Scheduler - schedules work crews based on availability.  
* Food & refreshments coordinator(s) - organizes the acquisition/preparation of food and refreshments for work crews.
* Safety Coordinator(s) - advises on the safe uses of tools, gathers safety material and distributes to authorized tool users.  
* Maintenance Coordinators(s) - advises on the proper maintenance of tools and organizes maintenance schedules.  




