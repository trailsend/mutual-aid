+++
fragment = "embed"
disabled = true
date = "2017-10-07"
weight = 120
background = "primary"

title = "Sign Up Now"
#subtitle = ""
#title_align = "left" # Default is center, can be left, right or center

# Embed a form via an iframe
# Mailerlite is one example of a working provider.
# There are others such as convertkit.
# Only necessity is for them to use iframes.
media = '<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeb7Roew_UrXUQ9jDhP2UuQK0QeYT5-fb4Ly5smjD8nLrnRHQ/viewform?embedded=true" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>'
responsive = true # prevent responsive behaviour
size = "100" # 25, 50, 75, 100 (percentage) - default: 75
+++